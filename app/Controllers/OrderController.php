<?php

    namespace App\Controllers;

    use Razorpay\Api\Api;
    use PDO;

    class OrderController extends Controller {
        protected $keyId = 'rzp_test_Xn9sbJ8SOJPlLq';
        protected $keySecret = 'uoXEmg5dpTQJuMdeDhP3pxjj';
        public function checkout($request, $response) {
            $cname = $_SESSION['customer']['cname'];
            $cid = $_SESSION['customer']['cid'];
            $sql = "select * from customer where customerId = '$cid'";
            $customer = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $c = $this->c->db->query($sql)->fetch();
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;
            $cartitems = array();
            $subTotal = 0;
            for($i=0;$i<$cart;$i++) {
                $pid = $_SESSION["cart"][$i]['productid'];
                $qty = $_SESSION["cart"][$i]['quantity'];
                $sql = "select productId, productName, productDescription, productImage1, productSP, '$qty' 
                        as quantityRequired from product where productId = '$pid'";
                $result = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
                $cartitem = $this->c->db->query($sql)->fetch();
                
                $subTotal = $subTotal + ($cartitem["productSP"] * $qty);
                $cartitems = array_merge($cartitems, $result);

                //var_dump($cartitem["productSP"]);
                //die();
            }

            return $this->c->view->render($response, 'Webpage/checkout.twig', [
                'subTotal' => $subTotal,
                'cartitems' => $cartitems,
                'customer' => $customer,
                'cname' => $cname,
                'cart' => $cart
            ]);
        }
        public function pay($request, $response, $args) {
            $params = $request->getParams();
            $contactNumber = $params["contactNumber"];
            $address = $params["address"];
            $cid = $_SESSION['customer']['cid'];
            $cname = $_SESSION['customer']['cname'];
            $sql = "select * from customer where customerId = '$cid'";
            $c = $this->c->db->query($sql)->fetch();
            $cartitems = array();
            $subTotal = 0;
            $totalcart = count($_SESSION["cart"]);
            for($i=0;$i<$totalcart;$i++) {
                $pid = $_SESSION["cart"][$i]['productid'];
                $qty = $_SESSION["cart"][$i]['quantity'];
                $sql = "select productId, productName, productDescription, productImage1, productSP, '$qty' 
                        as quantityRequired from product where productId = '$pid'";
                $result = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
                $cartitem = $this->c->db->query($sql)->fetch();
                
                $subTotal = $subTotal + ($cartitem["productSP"] * $qty);
                $cartitems = array_merge($cartitems, $result);

                //var_dump($cartitem["productSP"]);
                //die();
            }
            $api = new Api($this->keyId, $this->keySecret);

            $orderId = uniqid('ORDER_ID-', true);

            $totalAmount = $subTotal < 500 ? ($subTotal + 50) * 100 : $subTotal * 100;
            
            $orderData = [
                'receipt'         => $orderId,
                'amount'          => $totalAmount, // 2 rupees in paise
                'currency'        => 'INR',
                'payment_capture' => 1 // auto capture
            ];

            $razorpayOrder = $api->order->create($orderData);

            $razorpayOrderId = $razorpayOrder['id'];

            $amount = $orderData['amount'];

            $data = [
                "key"               => $this->keyId,
                "amount"            => $amount,
                "name"              => $cname,
                "description"       => "Payment for $totalcart items",
                "prefill"           => [
                "name"              => $c["customerName"],
                "email"             => $c["customerEmail"],
                "contact"           => $contactNumber,
                ],
                "notes"             => [
                "address"           => "$address",
                "merchant_order_id" => $orderId,
                ],
                "theme"             => [
                "color"             => "#59B210"
                ],
                "order_id"          => $razorpayOrderId,
            ];
            $json = json_encode($data);
            return $json;
        }

        public function paymentsuccess($request, $response, $args) {

            $api = new Api($this->keyId, $this->keySecret);
            $params = $request->getParams();
            
            $rzrpaysign = $params['rzrpaysign'];
            $rzrorderid = $params['rzrorderid'];

            $order = $api->order->fetch($rzrorderid)->payments();
            $invoiceNo = uniqid("INV-");
            $orderId = $order["items"][0]["notes"]["merchant_order_id"];
            $status = "CONFIRMED";
            $shippingAddress = $order["items"][0]["notes"]["address"];
            $amount = $order["items"][0]["amount"];
            $customerId = $_SESSION['customer']['cid'];
            $customerName = $_SESSION['customer']['cname'];
            $rzrpayid = $params['rzrpayid'];

            try {
                $sql = "insert into orders (orderId, orderStatus, orderShippingAddress, orderAmount, customerId) 
                        values ('$orderId', '$status', '$shippingAddress', '$amount', '$customerId' )";
                $this->c->db->exec($sql);
                $totalcart = count($_SESSION["cart"]);
                for($i=0;$i<$totalcart;$i++) {
                    $productId = $_SESSION["cart"][$i]['productid'];
                    $requiredQuantity = $_SESSION["cart"][$i]['quantity'];
                    $ocId = uniqid('',true);
                    $sql = "select productQuantity, productSP from product where productId = '$productId'";
                    $product = $this->c->db->query($sql)->fetch();
                    $productSP = $product['productSP'];
                    $pq = (int)$product['productQuantity'];
                    $rq = (int)$requiredQuantity;
                    $newproductQuantity = $pq - $rq;
                    $sql = "insert into orderscomprises (ocId, orderId, productId, productSP, productQuantity) 
                            values ('$ocId', '$orderId', '$productId', '$productSP', '$requiredQuantity')";
                    $this->c->db->exec($sql);
                    $sql = "update product set productQuantity = '$newproductQuantity' where productId = '$productId' ";
                    $this->c->db->exec($sql);
                }
                $sql = "insert into bill (invoiceNo, paymentId, orderId, paymentGatewayOrderId) 
                        values ('$invoiceNo', '$rzrpayid', '$orderId', '$rzrorderid')";
                $this->c->db->exec($sql);

                //unset($_SESSION["cart"]);

                if(isset($_SESSION["cart"]))
                    $cart = count($_SESSION["cart"]);
                else
                    $cart = 0;


                $url = $this->c->router->pathFor('order.paymentsuccesspage', ['orderid' => $orderId, 'rzrpayid' => $rzrpayid]);
                return  $url;
                //$response->withStatus(302);
                //return $response->withRedirect($this->c->router->pathFor('order.paymentsuccesspage', ['orderid' => $orderId, 'rzrpayid' => $rzrpayid]));

                // $paymentDetails = [
                //     "orderid" => $orderId,
                //     "rzrpayid" => $rzrpayid
                // ];
                // return json_encode($paymentDetails);


                // return $this->c->view->render($response, "Webpage/paymentsuccessful.twig", [
                //     'cart' => $cart,
                //     'cid' => $customerId,
                //     'cname' => $customerName,
                //     'orderId' => $orderId,
                //     'paymentId' => $rzrpayid,
                // ]);

            } catch (Exception $e) {
                echo 'Message: ' .$e->getMessage();
            }
        }
        

        public function paymentsuccesspage($request, $response, $args) {
            $cname = $_SESSION['customer']['cname'];
            $cid = $_SESSION['customer']['cid'];
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            //$params = $request->getParams();
            $orderId = $args['orderid'];
            $rzrpayid = $args['rzrpayid'];
            return $this->c->view->render($response, "Webpage/paymentsuccessful.twig", [
                'cart' => $cart,
                'cid' => $cid,
                'cname' => $cname,
                'orderId' => $orderId,
                'paymentId' => $rzrpayid,
            ]);

        }

        public function paymentfailure($request, $response, $args) {

        }

        public function yourorders($request, $response, $args) {
            $cname = $_SESSION['customer']['cname'];
            $cid = $_SESSION['customer']['cid'];
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;
            $sql = "select orders.orderId, orders.orderStatus, orders.orderShippingAddress, orders.orderAmount,
                        orders.customerId, orders.createdAt, orders.updatedAt, courierdetails.carrier, courierdetails.shippingService, 
                        courierdetails.trackingId, courierdetails.createdAt as arrivalDate from orders 
                        left join courierdetails on orders.orderId = courierdetails.orderId where 
                        customerId = '$cid' order by orders.createdAt DESC";
            
            //die();
            $orders = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    REPLACE(productName, ' ', '-') as productURL, (select AVG(review.reviewRating) from review 
                    where review.productId = product.productId and review.approvalStatus = '1') as avg from product 
                    where productActiveStatus = '1' order by RAND() LIMIT 10";
            $releatedProducts = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $index = sizeof($orders);
            $ordereditems = array();
            foreach ($orders as $item) {
                $orderId = $item->orderId;
                $sql = "select orderscomprises.ocId, orderscomprises.orderId, orderscomprises.productId, 
                        orderscomprises.productSP, orderscomprises.productQuantity , orderscomprises.createdAt, 
                        product.productName, product.productDescription, product.productImage1 from orderscomprises 
                        left join product on product.productId = orderscomprises.productId where orderId = '$orderId'";
                $orderscomprises = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
                $ordereditems = array_merge($ordereditems, $orderscomprises);
            }
            var_dump($orders);
            //die();
            return $this->c->view->render($response, "Webpage/yourorders.twig", [
                'cname' => $cname,
                'cid' => $cid,
                'cart' => $cart,
                'orders' => $orders,
                'releatedProducts' => $releatedProducts,
                'ordereditems' => $ordereditems
            ]);
        }

        public function viewpendingorders($request, $response, $args) {
            $sql = "select orders.orderId, orderStatus, orderShippingAddress, orderAmount, orders.createdAt, 
                    orders.customerId, product.productImage1, product.productName, COUNT(orderscomprises.productId) 
                    as quantity from orders left join orderscomprises on orders.orderId = orderscomprises.orderId
                    left join product on orderscomprises.productId = product.productId where orders.orderStatus = 'CONFIRMED'
                    or orders.orderStatus = 'DISPATCHED' group by orders.orderId order by createdAt DESC";
            $orders = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response, "/AdminPanel/viewpendingorders.twig", [
                'orders' => $orders
            ]);
        }

        public function vieworder($request, $response, $args) {
            $orderId = $args['orderid'];
            $sql = "select paymentGatewayOrderId from bill where orderId = '$orderId'";
            $paymentGatewayOrderId = $this->c->db->query($sql)->fetchColumn();
            $api = new Api($this->keyId, $this->keySecret);
            $paymentGatewayInfo = $api->order->fetch($paymentGatewayOrderId)->payments();
            $paymentMethods = ['card_id' => 'Card','bank' => 'Online Banking','wallet' => 'Wallet','vpa' => 'UPI'];
            foreach ($paymentMethods as $key => $value ) {
                if($paymentGatewayInfo['items'][0][$key] != '')
                    $paymentMethod = $paymentMethods[$key];
            }
            $sql = "select orders.orderId, orders.orderStatus, orders.orderShippingAddress, orders.orderAmount, 
                    customer.customerName, customer.customerEmail, customer.customerContactNumber, product.productName, 
                    orderscomprises.productSP, orderscomprises.productQuantity, product.productGSTTaxRate,
                    product.productHSNCode, courierdetails.carrier, courierdetails.shippingService, courierdetails.trackingId 
                    from orders left join customer on customer.customerId = orders.customerId
                    left join orderscomprises on orderscomprises.orderId = orders.orderId
                    left join product on product.productId = orderscomprises.productId
                    left join courierdetails on courierdetails.orderId = orders.orderId where orders.orderId = '$orderId'";
            $order = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            // var_dump($order);
            // die();
            return $this->c->view->render($response, "/AdminPanel/vieworder.twig", [
                'order' => $order,
                'paymentMethod' => $paymentMethod
            ]);
        }

        public function addcourierdetails($request, $response, $args) {
            $params = $request->getParams();
            $orderId = $params['orderid'];
            $carrier = $params['carrier'];
            $shippingservice = $params['shippingservice'];
            $trackingid = $params['trackingid'];

            echo $sql = "insert into courierdetails (carrier, shippingService, trackingId, orderId) value ('$carrier','$shippingservice','$trackingid','$orderId')";
            $this->c->db->exec($sql);

            $updatedAt = date("Y-m-d h:i:s", time());
            echo $sql = "update orders set orderStatus = 'DISPATCHED', updatedAt = '$updatedAt' where orderId = '$orderId'";
            $this->c->db->exec($sql);
        }

        public function ordershistory($request, $response, $args) {

            $sql = "select orders.orderId, orderStatus, orderShippingAddress, orderAmount, orders.createdAt, 
                    orders.customerId, product.productImage1, product.productName, COUNT(orderscomprises.productId) 
                    as quantity from orders left join orderscomprises on orders.orderId = orderscomprises.orderId
                    left join product on orderscomprises.productId = product.productId where orders.orderStatus = 'DELIVERED'
                    or orders.orderStatus = 'RETURNED' or orders.orderStatus = 'CANCELED' group by orders.orderId order by createdAt DESC";

            $orders = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            // var_dump($orders);
            // die();
            return $this->c->view->render($response, "/AdminPanel/ordershistory.twig", [
                'orders' => $orders                
            ]);
        }

        public function viewinvoice($request, $response, $args) {
            $orderId = $args['orderid'];
            $sql = "select orders.orderId, orders.orderStatus, orders.orderShippingAddress, orders.orderAmount, 
                    customer.customerName, customer.customerEmail, customer.customerContactNumber, product.productName, 
                    orderscomprises.productSP, orderscomprises.productQuantity, product.productGSTTaxRate,
                    product.productHSNCode, bill.invoiceNo from orders left join customer on 
                    customer.customerId = orders.customerId left join orderscomprises on 
                    orderscomprises.orderId = orders.orderId left join product on 
                    product.productId = orderscomprises.productId left join bill on
                    bill.orderId = orders.orderId where orders.orderId = '$orderId'";
            $order = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response, "/AdminPanel/invoice.twig",[
                'order' => $order
            ]);
        }

        public function printpackagingslip ($request, $response, $args) {
            $orderId = $args["orderid"];
            $sql = "select orders.orderId, orders.orderStatus, orders.orderShippingAddress, orders.orderAmount, orders.createdAt, 
                    customer.customerName, customer.customerEmail, customer.customerContactNumber, product.productName, 
                    orderscomprises.productSP, orderscomprises.productQuantity, product.productGSTTaxRate,
                    product.productHSNCode, bill.invoiceNo from orders left join customer on 
                    customer.customerId = orders.customerId left join orderscomprises on 
                    orderscomprises.orderId = orders.orderId left join product on 
                    product.productId = orderscomprises.productId left join bill on
                    bill.orderId = orders.orderId where orders.orderId = '$orderId'";
            $order = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            //var_dump($order);
            //die();
            return $this->c->view->render($response, "/AdminPanel/printpackagingslip.twig", [
                'order' => $order
            ]);
        }

        public function refundrequests($request, $response, $args) {
            $sql = "select orders.orderId, orderStatus, orderShippingAddress, orderAmount, orders.createdAt, 
                    orders.customerId, product.productImage1, product.productName, COUNT(orderscomprises.productId) 
                    as quantity from orders left join orderscomprises on orders.orderId = orderscomprises.orderId
                    left join product on orderscomprises.productId = product.productId where orders.orderStatus = 'RETURN'
                    group by orders.orderId order by createdAt DESC";

            $orders = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);

            //var_dump($orders);
            //die();

            return $this->c->view->render($response, "/AdminPanel/refundrequest.twig", [
                'orders' => $orders
            ]);
        }

        public function addcustomorder($request, $response, $args) {
            $params = $request->getParams();

            $customOrderId = uniqid('CUST-OD-');
            $customerId = $_SESSION['customer']['cid'];
            $customText = $params['text'];

            $filesUploaded = false;
            
            for($i=1; $i<=4; $i++) {
                if(isset($_FILES['customfile'.$i])) {
                    
                    $file = $_FILES['customfile'.$i];
                    $file_name = $file['name'];
                    $file_tmp = $file['tmp_name'];
                    $file_size = $file['size'];
                    $file_error = $file['error'];
            
            
                    $file_extension = explode('.', $file_name);
                    $file_ext = strtolower(end($file_extension));
            
                    $allowed = array('jpg','png','jpeg');
                    
                    if(in_array($file_ext, $allowed)) {
                        if($file_error === 0 && $file_size <= 2097152) {
                            $date = date('d-m-Y', time());
                            $new_file_name = 'public/customorderresources/'. uniqid('', true) .'.'. $file_ext;
                            echo $file_destination = __DIR__.'/../../'.$new_file_name;
            
                            $fileContent = file_get_contents($_FILES['customfile'.$i]['tmp_name']);
                                    
            
                            if(move_uploaded_file($file_tmp, $file_destination)) {
                                ${'customorder'.$i} = $new_file_name;
                                $filesUploaded = true;
                            }
                            else
                                $filesUploaded = false;
                        }
                    }
                }

            }

            if($filesUploaded) {

                $sql = "insert into customorder (customOrderId, customText, customOrderImage1, customOrderImage2, 
                        customOrderImage3, customOrderImage4, customerId) values ('$customOrderId','$customText',
                        '$customorder1','$customorder2','$customorder3','$customorder4','$customerId')";
                
                $this->c->db->exec($sql);

                return $response->withRedirect($this->c->router->pathFor('order.viewcustomorder'));
            }



        }

        public function managecustomorders($request, $response, $args) {
            $sql = "select * from customorder";
            $customorders = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);

            return $this->c->view->render($response, "AdminPanel/managecustomorders.twig", [
                'customorders' => $customorders 
            ]);
        }

        public function adminviewcustomorder($request, $response, $args) {
            echo $customOrderId = $args["customorderid"];
            $sql = "select * from customorder left join customer on customer.customerId = customorder.customerId 
                    where customOrderId = '$customOrderId'";
            $customorder = $this->c->db->query($sql)->fetch(PDO::FETCH_OBJ);
            return $this->c->view->render($response, "AdminPanel/viewcustomorder.twig", [
                'customorder' => $customorder,
            ]);
        }

        public function viewcustomorder($request, $response, $args) {
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            $cname = $_SESSION['customer']['cname'];
            $cid = $_SESSION['customer']['cid'];

            return $this->c->view->render($response, "Webpage/customorder.twig", [
                'cart' => $cart,
                'cname' => $cname,
                'cid' => $cid,
            ]);
        }

        public function returnorder($request, $response, $args) {
            $orderId = $args['orderid'];
            $sql = "update orders set orderStatus = 'RETURN' where orderId = '$orderId'";
            $this->c->db->query($sql);
            return $response->withRedirect($this->c->router->pathFor('order.yourorders'));
        }

        public function refundrequestorderdetails($request, $response, $args) {
            echo $orderId = $args['orderid'];
            $orderId = $args['orderid'];
            $sql = "select paymentGatewayOrderId from bill where orderId = '$orderId'";
            $paymentGatewayOrderId = $this->c->db->query($sql)->fetchColumn();
            $api = new Api($this->keyId, $this->keySecret);
            $paymentGatewayInfo = $api->order->fetch($paymentGatewayOrderId)->payments();
            $paymentMethods = ['card_id' => 'Card','bank' => 'Online Banking','wallet' => 'Wallet','vpa' => 'UPI'];
            foreach ($paymentMethods as $key => $value ) {
                if($paymentGatewayInfo['items'][0][$key] != '')
                    $paymentMethod = $paymentMethods[$key];
            }
            $sql = "select orders.orderId, orders.orderStatus, orders.orderShippingAddress, orders.orderAmount, 
                    customer.customerName, customer.customerEmail, customer.customerContactNumber, product.productName, 
                    orderscomprises.productSP, orderscomprises.productQuantity, product.productGSTTaxRate,
                    product.productHSNCode from orders left join customer on customer.customerId = orders.customerId
                    left join orderscomprises on orderscomprises.orderId = orders.orderId
                    left join product on product.productId = orderscomprises.productId where orders.orderId = '$orderId'";
            $order = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            //var_dump($order);
            //die();
            return $this->c->view->render($response, "/AdminPanel/refundrequestorderdetails.twig", [
                'order' => $order,
                'paymentMethod' => $paymentMethod
            ]);
        }

        public function orderdelivered($request, $response, $args) {
            $params = $request->getParams();
            $orderId = $params["orderid"];
            $updatedAt = date("Y-m-d h:i:s", time());
            echo $sql = "update orders set orderStatus = 'DELIVERED', updatedAt = '$updatedAt' where orderId = '$orderId'";
            $this->c->db->exec($sql);
        }


        public function orderreturned($request, $response, $args) {
            $params = $request->getParams();
            $orderId = $params["orderid"];
            $updatedAt = date("Y-m-d h:i:s", time());
            echo $sql = "update orders set orderStatus = 'RETURNED', updatedAt = '$updatedAt' where orderId = '$orderId'";
            $this->c->db->exec($sql);
        }

        public function ordercanceled($request, $response, $args) {
            $params = $request->getParams();
            $orderId = $params["orderid"];
            $updatedAt = date("Y-m-d h:i:s", time());
            $sql = "update orders set orderStatus = 'CANCELED', updatedAt = '$updatedAt' where orderId = '$orderId'";
            $this->c->db->exec($sql);
            return $response->withRedirect($this->c->router->pathFor('order.yourorders'));
        }

        public function cancelorder($request, $response, $args) {
            $orderId = $args["orderid"];
            $updatedAt = date("Y-m-d h:i:s", time());
            $sql = "update orders set orderStatus = 'CANCELED', updatedAt = '$updatedAt' where orderId = '$orderId'";
            $this->c->db->exec($sql);
            return $response->withRedirect($this->c->router->pathFor('order.yourorders'));
        }

        public function initiaterefund($request, $response, $args) {
            
        }
    }