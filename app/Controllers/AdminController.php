<?php

    namespace App\Controllers;

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\IOFactory;
    use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
    use PhpOffice\PhpSpreadsheet\Helper\Sample;
    use PDO;
    
    //use Slim\Http\UploadedFile;

    class AdminController extends Controller {

        protected $sql;

        public function index($request, $response) {
            
            $sql = "select reviewMessage, reviewRating, product.productName, reviewCustomerName from review left join product on product.productId = review.productId left join customer on customer.customerId = review.customerId order by review.createdAt DESC LIMIT 7";
            $reviews = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select count(orderId) from orders where orderStatus = 'CONFIRMED'";
            $totalOrders = $this->c->db->query($sql)->fetchColumn();
            $sql = "select sum(orderAmount) from orders where orderStatus = 'DISPATCHED'";
            $totalRevenue = $this->c->db->query($sql)->fetchColumn();
            $sql = "select orders.orderId, orderStatus, orderShippingAddress, orderAmount, orders.createdAt, 
                    orders.customerId, product.productImage1, product.productName, COUNT(orderscomprises.productId) 
                    as quantity from orders left join orderscomprises on orders.orderId = orderscomprises.orderId
                    left join product on orderscomprises.productId = product.productId where orders.orderStatus = 'CONFIRMED'
                    group by orders.orderId order by createdAt DESC LIMIT 7";
            $orders = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            
            return $this->c->view->render($response, 'AdminPanel/index.twig', [
                'reviews' => $reviews,
                'totalOrders' => $totalOrders,
                'totalRevenue' => $totalRevenue,
                'orders' => $orders
            ]);
        }

        public function signup($request, $response) {
            return "Signup";
        }

        public function uploadbluk($request, $response) {
            return $this->c->view->render($response, '/AdminPanel/uploadbulkproducts.twig');
        }

        public function uploadsingle($request, $response) {
            return $this->c->view->render($response, '/AdminPanel/uploadsingleproduct.twig');
        }

        public function editproducts($request, $response) {
            $sql = 'select productId, productName, productMRP, productSP, productQuantity, 
                    productActiveStatus, productImage1 from product';
            $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response, '/AdminPanel/editproducts.twig', compact('products'));
        }

        public function editproduct($request, $response, $args) {
            $productId = $args['id'];
            $sql = "select * from product where productId = '$productId'";
            $product = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response, '/AdminPanel/editsingleproduct.twig', compact('product'));
        }

        public function updateinventory($request, $response) {
            return $this->c->view->render($response, '/AdminPanel/updateinventory.twig');
        }

        public function removeproducts($request, $response) {
            $sql = "select productId, productName, productSP, productQuantity, productImage1 from product";
            $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response, '/AdminPanel/removeproducts.twig', compact('products'));
        }

        public function productdelete($request, $response) {
            $params = $request->getParams();
            $productId = $params["productId"];
            // $sql = "select * from product where productId = '$productId'";
            // $products = $this->c->db->query($sql);
            // foreach ($products as $product) {
            //     echo $productImage1 = __DIR__.'/../../'. $product["productImage1"];
            //     echo $productImage2 = __DIR__.'/../../'. $product["productImage2"];
            //     echo $productImage3 = __DIR__.'/../../'. $product["productImage3"];
            //     echo $productImage4 = __DIR__.'/../../'. $product["productImage4"];
            //     echo $productImage5 = __DIR__.'/../../'. $product["productImage5"];
            // }
            // $products->exec();
            // for ($i=1;$i<=5;$i++) {
            //     unlink(${'productImage' . $i});
            // }
            $sql = "delete from product where productId = '$productId';";
            $this->c->db->query($sql)->exec();
        }

        public function updateproductstatus($request, $response, $args) {
            $params = $request->getParams();
            $productId = $params["productId"];
            $status = $params["status"];
            $sql = "update product set productActiveStatus = '$status' where productId = '$productId'";
            $this->c->db->query($sql)->exec();
        }

        public function updateproductmrp($request, $response, $args) {
            $params = $request->getParams();
            $productId = $params["productId"];
            $mrp = $params["mrp"];
            $sql = "update product set productMRP = '$mrp' where productId = '$productId'";
            $this->c->db->query($sql)->exec();
        }

        public function updateproductsp($request, $response, $args) {
            $params = $request->getParams();
            $productId = $params["productId"];
            $sp = $params["sp"];
            $sql = "update product set productSP = '$sp' where productId = '$productId'";
            $this->c->db->query($sql)->exec();
        }

        public function updateproductquantity($request, $response, $args) {
            $params = $request->getParams();
            $productId = $params["productId"];
            $qty = $params["qty"];
            $sql = "update product set productQuantity = '$qty' where productId = '$productId'";
            $this->c->db->query($sql)->exec();
        }
        

        public function trial($request, $response, $args) {
            $params = $request->getParams();
            $productId = $params["productId"];
            $status = $params["status"];
            return $this->c->view->render($response, '/AdminPanel/trial.twig', [
                'productId' => $productId,
                'status' => $status
            ]);
        }

        public function uploadExcel($request, $response) {
            if(isset($_FILES['excelfile'])) {
                $file = $_FILES['excelfile'];
                $file_name = $file['name'];
                $file_tmp = $file['tmp_name'];
                $file_size = $file['size'];
                $file_error = $file['error'];
        
        
                $file_extension = explode('.', $file_name);
                $file_ext = strtolower(end($file_extension));
        
                $allowed = array('xlsx','xls');
                
                if(in_array($file_ext, $allowed)) {
                    if($file_error === 0 && $file_size <= 2097152) {
                        $date = date('d-m-Y', time());
                        $new_file_name = uniqid('', true) .'-date-'. $date .'.'. $file_ext;
                        $file_destination = __DIR__.'\\..\\..\\public\\products\\productsExcel\\'.$new_file_name;
        
                        $fileContent = file_get_contents($_FILES['excelfile']['tmp_name']);
                                
        
                        if(move_uploaded_file($file_tmp, $file_destination)) {
                            $inputFileType = 'Xlsx';
                            $inputFileName = $file_destination;
            
                            $reader = IOFactory::createReader($inputFileType);
                            $reader->setReadDataOnly(true);
                            $spreadsheet = $reader->load($inputFileName);
            
                            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                            $sqlQuery= "";
                            for($row = 2; $row <= count($sheetData); $row++) {//
                                $imagesUploaded = false;

                                $productId = uniqid('PROD-', true);
                                $productName = $sheetData[$row]['A'];
                                $productType = $sheetData[$row]['B'];
                                $productQuantity = $sheetData[$row]['C'];
                                $productMRP = $sheetData[$row]['D'];
                                $productSP = $sheetData[$row]['E'];
                                $productColor = $sheetData[$row]['F'];
                                $productMaterialType = $sheetData[$row]['G'];
                                $productHSNCode = $sheetData[$row]['H'];
                                $productActiveStatus = '1';
                                $productGSTTaxRate = $sheetData[$row]['I'];
                                $productCarMake = $sheetData[$row]['J'];
                                $productCarModel = $sheetData[$row]['K'];
                                $productDescription = $sheetData[$row]['L'];
                                $productBulletPoint1 = $sheetData[$row]['M'];
                                $productBulletPoint2 = $sheetData[$row]['N'];
                                $productBulletPoint3 = $sheetData[$row]['O'];
                                $productBulletPoint4 = $sheetData[$row]['P'];
                                $productBulletPoint5 = $sheetData[$row]['Q'];
                                                                
                                
                                //$values = "'" . implode("','", $sheetData[$row]['F']) . "'";
                                $image1 = $sheetData[$row]['R'] == '' ? '' : substr($sheetData[$row]['R'], 0, -4).'raw=1';
                                $image2 = $sheetData[$row]['S'] == '' ? '' : substr($sheetData[$row]['S'], 0, -4).'raw=1';
                                $image3 = $sheetData[$row]['T'] == '' ? '' : substr($sheetData[$row]['T'], 0, -4).'raw=1';
                                $image4 = $sheetData[$row]['U'] == '' ? '' : substr($sheetData[$row]['U'], 0, -4).'raw=1';
                                $image5 = $sheetData[$row]['V'] == '' ? '' : substr($sheetData[$row]['V'], 0, -4).'raw=1';
                                

                                for($i = 1;$i<=5;$i++) {
                                    header("Content-type: image/jpg");
                                    if(${'image' . $i} != '') {
                                        ${'content' . $i} = file_get_contents(${'image' . $i});
                                        ${'uploadedImage' . $i} = 'public/products/productImages/' . uniqid('', true) .'.jpg';
                                        file_put_contents( __DIR__.'/../../' . ${'uploadedImage' . $i} , ${'content' . $i});
                                        $imagesUploaded = true;
                                    }
                                    else {
                                        ${'uploadedImage' . $i} = '';
                                    }
                                    
                                }
                                if($imagesUploaded == true) {
                                    $sql = "insert into product (productId,productName,productType,
                                    productQuantity,productMRP,productSP,productColor,productMaterialType,
                                    productHSNCode,productActiveStatus,productGSTTaxRate,productCarMake,productCarModel,
                                    productDescription,productBulletPoint1,productBulletPoint2,productBulletPoint3,productBulletPoint4,
                                    productBulletPoint5,productImage1,productImage2,productImage3,productImage4,productImage5) values 
                                    ('$productId','$productName','$productType',
                                    '$productQuantity','$productMRP','$productSP','$productColor','$productMaterialType',
                                    '$productHSNCode','$productActiveStatus','$productGSTTaxRate','$productCarMake','$productCarModel',
                                    '$productDescription','$productBulletPoint1','$productBulletPoint2','$productBulletPoint3','$productBulletPoint4',
                                    '$productBulletPoint5','$uploadedImage1','$uploadedImage2','$uploadedImage3','$uploadedImage4','$uploadedImage5')";

                                    $this->c->db->exec($sql);                                    
                                }
                            }
                            return $response->withRedirect($this->c->router->pathFor('admin.uploadbluk'));
                        }
                    }
                }
            }
             
        }

        public function uploadsingleproduct($request, $response) {
            $params = $request->getParams();
            $imageUploaded = false;
            
            for($i=1; $i<=5; $i++) {
                if(isset($_FILES['productImage'.$i])) {
                    $file = $_FILES['productImage'.$i];
                    $file_name = $file['name'];
                    $file_tmp = $file['tmp_name'];
                    $file_size = $file['size'];
                    $file_error = $file['error'];
            
            
                    $file_extension = explode('.', $file_name);
                    $file_ext = strtolower(end($file_extension));
            
                    $allowed = array('jpg','png','jpeg');
                    
                    if(in_array($file_ext, $allowed)) {
                        if($file_error === 0 && $file_size <= 2097152) {
                            $date = date('d-m-Y', time());
                            $new_file_name = 'public/products/productImages/'. uniqid('', true) .'.'. $file_ext;
                            $file_destination = __DIR__.'/../../'.$new_file_name;
            
                            $fileContent = file_get_contents($_FILES['productImage'.$i]['tmp_name']);
                                    
            
                            if(move_uploaded_file($file_tmp, $file_destination)) {
                                ${'uploadedImage'.$i} = $new_file_name;
                                $imageUploaded = true;
                            }
                            else
                                $imageUploaded = false;
                        }
                    }
                }

            }

            if($imageUploaded == true) {
                $productId = uniqid('PROD-', true);
                $productName = $params['productName'] ;
                $productType = $params['productType'];
                $productQuantity = $params['productQuantity'];
                $productMRP = $params['productMRP'];
                $productSP = $params['productSP'];
                $productColor = $params['productColor'];
                $productMaterialType = $params['productMaterialType'];
                $productHSNCode = $params['productHSNCode'];
                $productActiveStatus = '1';
                $productGSTTaxRate = $params['productGSTTaxRate'];
                $productCarMake = $params['productCarMake'];
                $productCarModel = $params['productCarModel'];
                $productDescription = $params['productDescription'];
                $productBulletPoint1 = $params['productBulletPoint1'];
                $productBulletPoint2 = $params['productBulletPoint2'];
                $productBulletPoint3 = $params['productBulletPoint3'];
                $productBulletPoint4 = $params['productBulletPoint4'];
                $productBulletPoint5 = $params['productBulletPoint5'];
                
                $sql = "insert into product (productId,productName,productType,
                        productQuantity,productMRP,productSP,productColor,productMaterialType,
                        productHSNCode,productActiveStatus,productGSTTaxRate,productCarMake,productCarModel,
                        productDescription,productBulletPoint1,productBulletPoint2,productBulletPoint3,productBulletPoint4,
                        productBulletPoint5,productImage1,productImage2,productImage3,productImage4,productImage5) values 
                        ('$productId','$productName','$productType',
                        '$productQuantity','$productMRP','$productSP','$productColor','$productMaterialType',
                        '$productHSNCode','$productActiveStatus','$productGSTTaxRate','$productCarMake','$productCarModel',
                        '$productDescription','$productBulletPoint1','$productBulletPoint2','$productBulletPoint3','$productBulletPoint4',
                        '$productBulletPoint5','$uploadedImage1','$uploadedImage2','$uploadedImage3','$uploadedImage4','$uploadedImage5')";

                $this->c->db->exec($sql);
            }
            return $response->withRedirect($this->c->router->pathFor('admin.uploadsingle'));
        }
    }