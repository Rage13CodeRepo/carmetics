<?php

    namespace App\Controllers;

    use PDO;

    class CustomerController extends Controller{
        public function login($request, $response) {
            return $this->c->view->render($response, 'Webpage/login.twig');
        }

        public function signup($request, $response) {
            // echo "Done1";
            // $response->getBody()->write(' Wha What !!! ');
            return $this->c->view->render($response, 'Webpage/signup.twig');
        }
        
        public function home($request, $response) {
            
            if(isset($_SESSION['customer']['cid'])) {
                $cid = $_SESSION['customer']['cid'];
                $cname = $_SESSION['customer']['cname'];
            }
            else {
                $cid = NULL;
                $cname = '';
            }

            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            // var_dump($cart);
            // die();
            $productType = "ALL CATEGORIES";

            $sql = "select productCarMake from product group by productCarMake order by RAND() LIMIT 28";
            $carmake = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productCarModel from product group by productCarModel order by RAND() LIMIT 28";
            $carmodel = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            // var_dump($carmake);
            // die();
            $sql = "select productType from product group by productType LIMIT 3";
            $categories = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId
                     and review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL 
                     from product where productActiveStatus = '1' order by createdAt, RAND()  DESC LIMIT 6;";
            $allnewarrivals = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId
                     and review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL 
                     from product where productActiveStatus = '1' and productType like '%Stickers%' 
                     order by createdAt, RAND() DESC LIMIT 6;";
            $stickersnewarrivals = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId 
                    and review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL  
                    from product where productActiveStatus = '1' and productType like '%emblem%'
                     order by createdAt, RAND() DESC LIMIT 6;";
            $emblemsnewarrivals = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId 
                    and review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL
                     from product where productActiveStatus = '1' and productType like '%logo%' 
                     order by createdAt, RAND() DESC LIMIT 6;";
            $logosnewarrivals = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId
                     and review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL 
                     from product where productActiveStatus = '1' order by productSP DESC LIMIT 6;";
            $bestsellers = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    concat(round(100 - (productSP/productMRP * 100),2),'%') as percentage, REPLACE(productName, ' ', '-') 
                    as productURL, (select AVG(review.reviewRating) from review where review.productId = product.productId 
                    and review.approvalStatus = '1') as avg from product where productActiveStatus = '1' 
                    order by percentage ASC LIMIT 9;";
            $specialdeals = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            // $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
            //         (select AVG(review.reviewRating) from review where review.productId = product.productId 
            //         and review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL
            //          from product where productActiveStatus = '1' order by productSP ASC LIMIT 9;";
            // $specialOffers = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            // var_dump($carmake);
            // die();
            return $this->c->view->render($response, 'Webpage/home.twig', [
                'cid' => $cid,
                'cname' => $cname,
                'cart' => $cart,
                'carmake' => $carmake,
                'carmodel' => $carmodel,
                'categories' => $categories,
                'bestsellers' => $bestsellers,
                'allnewarrivals' => $allnewarrivals,
                'stickersnewarrivals' => $stickersnewarrivals,
                'emblemsnewarrivals' => $emblemsnewarrivals,
                'logosnewarrivals' => $logosnewarrivals,
                'specialdeals' => $specialdeals,
                'productType' => $productType
                //'specialOffers' => $specialOffers,
            ]);            
        }

        public function GoogleSignIn($request, $response) {
            $config = [
                'callback' => 'http://localhost/Carmetics/public/GoogleSignin',
                'keys'     => [ 'id' => '465780052885-pd2nbfadsfk0fv7hn7bcrd84gff40b4t.apps.googleusercontent.com', 'secret' => 'M-ucqprjvnGySMn24aWo7ofZ' ],
                
            ];
            
            try {
                $adapter = new \Hybridauth\Provider\Google( $config );
                $adapter->authenticate();
        
                $userProfile = $adapter->getUserProfile();
                $cid = $userProfile->identifier;
                $name = $userProfile->firstName . ' ' .  $userProfile->lastName;
                $username = $userProfile->displayName;
                $email = $userProfile->email;
                $authProvider = "Google";
                $sql = "select count(*) from customer where customerId = '$cid'";
                $row = $this->c->db->query($sql)->fetchColumn();
                
                if($row == 0) {
                    $sql = "insert into customer (customerId, customerName, customerUsername, customerEmail, 
                            customerContactNumber, customerAddress, customerPassword, authProvider) 
                            values('$cid','$name','$username','$email',NULL,NULL,NULL,'$authProvider')";
                    $this->c->db->exec($sql);
                    $_SESSION['customer']['cid'] = $cid;
                    $_SESSION['customer']['cname'] = $username;
                }
                else {
                    $sql = "select * from customer where customerId = '$cid'";
                    $customer = $this->c->db->query($sql)->fetch();
                    $_SESSION['customer']['cid'] = $customer["customerId"];
                    $_SESSION['customer']['cname'] = $customer["customerName"];
                }

                $request->withAttribute('Userid', $_SESSION['customer']['cid']);

                $adapter->disconnect();
            }
            catch(\Exception $e){
                echo 'Oops, we ran into an issue! ' . $e->getMessage();
            }
        }

        public function FacebookSignIn($request, $response) {
            $config = [
                'callback' => 'https://localhost/Carmetics/public/FacebookSignin',
                'keys'     => [ 'id' => '1886551614783733', 'secret' => '5fe019c38d6d0b7348aaa43f2a070841' ]
            ];
        
            try {

                $adapter = new \Hybridauth\Provider\Facebook($config);
                $adapter->authenticate();
        
                $userProfile = $adapter->getUserProfile();
                echo $cid = $userProfile->identifier;
                echo $name = $userProfile->firstName . ' ' .  $userProfile->lastName;
                echo $username = $userProfile->displayName;
                echo $email = $userProfile->email;
                echo $authProvider = "Facebook";

                $sql = "select count(*) from customer where customerId = '$cid'";
                $row = $this->c->db->query($sql)->fetchColumn();
                
                if($row == 0) {
                    $sql = "insert into customer (customerId, customerName, customerUsername, customerEmail, 
                            customerContactNumber, customerAddress, customerPassword, authProvider) 
                            values('$cid','$name','$username','$email',NULL,NULL,NULL,'$authProvider')";
                    $this->c->db->exec($sql);
                    $_SESSION['customer']['cid'] = $cid;
                    $_SESSION['customer']['cname'] = $username;
                }
                else {
                    $sql = "select * from customer where customerId = '$cid'";
                    $customer = $this->c->db->query($sql)->fetch();
                    $_SESSION['customer']['cid'] = $customer["customerId"];
                    $_SESSION['customer']['cname'] = $customer["customerName"];
                }
                var_dump($_SESSION['customer']);
                //die();
                $adapter->disconnect();
            }
            catch( Exception $e ){
                echo $e->getMessage() ;
            }
        }

        public function carmeticssignup($request, $response) {
            $params = $request->getParams();
            $email = $params["email"];
            $name = $params["name"];
            $password = $params["password"];
            $confirmPassword = $params["confirmPassword"];

            $sql = "select count(*) from customer where customerEmail = '$email'";
            $row = $this->c->db->query($sql)->fetchColumn();
            if($row == 0) {
                if($password == $confirmPassword) {
                    $id = uniqid('', true);
                    $encPwd = md5($confirmPassword);
                    $sql = "insert into customer (customerId, customerName, customerUsername, customerEmail, 
                            customerContactNumber, customerAddress, customerPassword, authProvider) 
                            values('$id','$name','$name','$email',NULL,NULL,'$encPwd','carmetics')";
                    $this->c->db->exec($sql);
                    $_SESSION['customer']['cid'] = $id;
                    $_SESSION['customer']['cname'] = $name;
                    $response->getBody()->write("First");
                    //return $response->withRedirect($this->c->router->pathFor('customer.home'));
                    $request->withAttribute('Userid', $_SESSION['customer']['cid']);
                }
            }
        }

        public function carmeticslogin($request, $response) {
            $params = $request->getParams();
            $email = $params["email"];
            $password = $params["password"];

            $encPwd = md5($password);
            $sql = "select count(*) from customer where customerEmail = '$email' and customerPassword = '$encPwd'";
            echo $row = $this->c->db->query($sql)->fetchColumn();
            if($row == 1) {
                $sql = "select * from customer where customerEmail = '$email' and customerPassword = '$encPwd'";
                $customer = $this->c->db->query($sql)->fetch();
                $_SESSION['customer']['cid'] = $customer["customerId"];
                $_SESSION['customer']['cname'] = $customer["customername"];
                $request->withAttribute('Userid', $_SESSION['customer']['cid']);
            }
        }

        public function logout($request, $response) {
            unset($_COOKIE['AccessToken']);
            unset($_SESSION['customer']);
            setcookie('AccessToken', '', time() - (86400 * 30), "/");
            
            return $response->withRedirect($this->c->router->pathFor('customer.home'));
        }

        public function myaccount($request, $response, $args) {
            $cname = $_SESSION['customer']['cname'];
            $cid = $_SESSION['customer']['cid'];
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            $sql = "select * from customer where customerId = '$cid'";
            $customer = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);

            return $this->c->view->render($response, "Webpage/myaccount.twig", [
                'cname' => $cname,
                'cid' => $cid,
                'cart' => $cart,
                'customer' => $customer
            ]);
        }

        public function updateaccountinfo($request, $response, $args) {
            $params = $request->getParams();
            $cid = $_SESSION['customer']['cid'];
            $cn = $params["cn"];
            $name = $params["name"];
            $address = $params["address"];

            $sql = "update customer set customerName = '$name', customerContactNumber = '$cn', 
                    customerAddress = '$address' where customerId = '$cid'";
            $this->c->db->exec($sql);
            return $response->withRedirect($this->c->router->pathFor('customer.myaccount'));
        }

        public function updatepassword($request, $response, $args) {
            $params = $request->getParams();
            $cid = $_SESSION['customer']['cid'];
            $currentPassword = $params['currentPassword'];
            $newPassword = $params['newPassword'];
            $confirmPassword = $params['confirmPassword'];
            $encCurrentPassword = md5($currentPassword);
            echo $sql = "select count(*) from customer where customerId = '$cid' and 
                        customerPassword = '$encCurrentPassword'";
            $row = $this->c->db->query($sql)->fetchColumn();
            if($row == 1) {
                if($newPassword == $confirmPassword) {
                    $newEncPassword = md5($confirmPassword);
                    $sql = "update customer set customerPassword = '$newEncPassword' where customerId = '$cid'";
                    $this->c->db->exec($sql);
                }
            }
            return $response->withRedirect($this->c->router->pathFor('customer.myaccount'));
        }

        public function viewcustomers($request, $response, $args) {
            $sql = "select * from customer";
            $customers = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);

            return $this->c->view->render($response, "/AdminPanel/manageusers.twig", [
                'customers' => $customers
            ]);
        }
    }