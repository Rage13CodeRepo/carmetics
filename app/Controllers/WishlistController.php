<?php

    namespace App\Controllers;

    use PDO;

    class WishlistController extends Controller{

        public function addtowishlist($request, $response) {
            $params = $request->getParams();
            $cid = $params["customerId"];
            $pid = $params["productId"];

            $sql = "insert into wishlist (customerId, productId) values ('$cid','$pid')";
            $this->c->db->exec($sql);
        }

        public function viewwishlist($request, $response, $args) {
            $params = $request->getParams();
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            if(isset($_SESSION['customer']['cid'])) {
                $cid = $_SESSION['customer']['cid'];
                $cname = $_SESSION['customer']['cname'];
            }
            else {
                $cid = NULL;
                $cname = NULL;
            }

            $sql = "select wishlist.productId, product.productName, product.productImage1, product.productSP, 
                    REPLACE(product.productName, ' ', '-') as productURL, (select AVG(review.reviewRating) from review 
                    where review.productId = product.productId and review.approvalStatus ='1')as avg, 
                    (select COUNT(review.reviewRating) from review where review.productId = product.productId and review.approvalStatus ='1')
                    as totalreviews from wishlist left join product on product.productId = wishlist.productId 
                    left join review on review.productId = wishlist.productId where wishlist.customerId = '$cid'";
            $wishlist = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            //var_dump($wishlist);
            //die();
            return $this->c->view->render($response, "Webpage/wishlist.twig", [
                'cart' => $cart,
                'cid' => $cid,
                'cname' => $cname,
                'wishlist' => $wishlist
            ]);
        }

        public function deletefromwishlist($request, $response, $args) {
            if(isset($_SESSION['customer']['cid'])) 
                $cid = $_SESSION['customer']['cid'];
            $params = $request->getParams();
            $productId = $params['productId'];
            echo $sql = "delete from wishlist where customerId = '$cid' and productId = '$productId'";
            //die();
            $this->c->db->exec($sql);
        }
    }