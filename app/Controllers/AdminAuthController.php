<?php

    namespace App\Controllers;

    use PDO;

    class AdminAuthController extends Controller{
        public function login($request, $response) {
            
            // unset($_SESSION['admin']);
            // die();
            return $this->c->view->render($response, 'AdminPanel/login.twig');
        }

        public function adminlogin($request, $response, $args) {
            
            $params = $request->getParams();
            $email = $params["email"];
            $password = $params["password"];

            $encpassword = md5($password);
            $sql = "select adminId, adminName, count(*) as rows from admin where adminEmail = '$email' and adminPassword = '$encpassword'";
            $admin = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $admin = $this->c->db->query($sql)->fetch();
            if($admin['rows'] == 1) {
                $_SESSION['admin']['aid'] = $admin["adminId"];
                $_SESSION['admin']['aname'] = $admin["adminName"];
            }

        }
        public function logout($request, $response, $args) {
            unset($_SESSION['admin']);
            setcookie('AdminAccessToken', '', time() - (86400 * 30), "/");
            return $response->withRedirect($this->c->router->pathFor('adminauth.login'));
        }
    }