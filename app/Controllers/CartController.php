<?php

namespace App\Controllers;

use PDO;

class CartController extends Controller {
    public function addtocart($request, $response) {
        $params = $request->getParams();
        $productId = $params["productId"];
        $productQty = $params["productQty"];
        count($_SESSION["cart"]);
        if ($productId != NULL) {
            if (isset($_SESSION["customer"])) {
                if (is_array($_SESSION["cart"])) {
                    $li = count($_SESSION["cart"]);
                    $nli = $li + 1;
                    for ($i = 0; $i < $li; $i++) {
                        if ($productId != $_SESSION["cart"][$i]["productid"]) {
                            $flag = 1;
                        } else {
                            $flag = 0;
                        }
                    }
                    if ($flag == 1) {
                        $_SESSION["cart"][$li]["productid"] = $productId;
                        $_SESSION["cart"][$li]["quantity"] = $productQty;
                    }
                } else {
                    $_SESSION["cart"] = array();
                    $_SESSION["cart"][0]["productid"] = $productId;
                    $_SESSION["cart"][0]["quantity"] = $productQty;
                }
                //var_dump($_SESSION["cart"]);
                //die();
            }
        }
    }

    public function viewcart($request, $response, $args) {
        // unset($_SESSION["cart"]);
        // var_dump($_SESSION["cart"]);
        // die();
        // var_dump($_SESSION['cart']);
        $subTotal = 0;
        $cname = $_SESSION['customer']['cname'];
        if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
        else
            $cart = 0;
        $cartitems = array();
        if($cart > 0) {
            for($i=0;$i<$cart;$i++) {
                $pid = $_SESSION["cart"][$i]['productid'];
                $qty = $_SESSION["cart"][$i]['quantity'];
                $sql = "select productId, productName, productImage1, productSP, '$qty' as quantityRequired, 
                        (select AVG(review.reviewRating) from review where review.productId = product.productId and review.approvalStatus ='1')
                        as avg, (select COUNT(review.reviewRating) from review where review.productId = product.productId and review.approvalStatus = '1') 
                        as totalreviews, REPLACE(productName, ' ', '-') as productURL from product where productId = '$pid'";
                $result = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
                $cartitem = $this->c->db->query($sql)->fetch();
                
                $subTotal = $subTotal + ($cartitem["productSP"] * $qty);
                $cartitems = array_merge($cartitems, $result);
                //var_dump($cartitem["productSP"]);
                //die();
            }
        }
        
        //var_dump($subTotal);
        return $this->c->view->render($response, "Webpage/cart.twig", [
            'cname' => $cname,
            'cart' => $cart,
            'cartitems' => $cartitems,
            'subTotal' => $subTotal,
        ]);
    }

    public function updatecart($request, $response, $args) {
        $params = $request->getParams();
        $productId = $params["productId"];
        $requiredQuantity = $params["requiredQuantity"];
        for($i=0;$i<count($_SESSION["cart"]);$i++) {
            if($_SESSION["cart"][$i]["productid"] == $productId) {
                $_SESSION["cart"][$i]["quantity"] = $requiredQuantity;
            }
        }        
    }

    public function deletefromcart($request, $response, $args) {
        $params = $request->getParams();
        $productId = $params["productId"];
        $totalcart = count($_SESSION["cart"]);
        //die();
        if($totalcart == 1) {
            unset($_SESSION["cart"]);
        }
        for($i=0;$i<$totalcart;$i++) {
            if($_SESSION["cart"][$i]["productid"] == $productId) {
                unset($_SESSION["cart"][$i]);
            }
        }
        $cart = array_values($_SESSION["cart"]);
        $_SESSION['cart'] = $cart;
        
    }

    public function updatecartdisplayquantity($request, $response, $args) {
        if(isset($_SESSION['cart']))
            $count = count($_SESSION['cart']);
        else
            $count = 0;

        //echo $count;
        //die();
        $result = json_encode($count);
        return $result;
    }
}