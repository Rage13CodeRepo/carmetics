<?php

    namespace App\Controllers;

    use PDO;

    class ProductController extends Controller{
        public function getcarmodellist($request, $response) {
            $param = $request->getParams();
            $carmake = $param['carmake'];
            $sql = "select productCarModel from product where productCarMake = '$carmake' group by productCarModel";
            $product = $this->c->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $result = json_encode($product);
            //var_dump($result);
            //die();
            return $result;
        }

        public function showproductdetails($request, $response, $args) {
            
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            $name =  $args['name'];
            $id = $args['id'];
            $sql = "select *, product.productId, concat(round(100 - (productSP/productMRP * 100),2),'%') as 
                    productPercentage, count(review.reviewMessage) as totalreview, AVG(reviewRating) as averageRating 
                    from product left join review on review.productId = product.productId where product.productId = '$id'
                    and review.approvalStatus = '1'";         
            $product = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $pt = $this->c->db->query($sql)->fetch();
            $productType = $pt['productType'];
            $sql = "select productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    REPLACE(productName, ' ', '-') as productURL, (select AVG(review.reviewRating) from review 
                    where review.productId = product.productId and review.approvalStatus = '1') as avg from product where productType = '$productType' 
                    or productType = '%Universal%' and productActiveStatus = '1' LIMIT 10";
            $releatedProducts = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select * from review where productId = '$id' and approvalStatus = '1'";
            $reviews = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            $sql = "select COUNT(*) from orders, orderscomprises where orders.orderId = orderscomprises.orderId and 
                    orderscomprises.productId = '$id'";
            $ordered = $this->c->db->query($sql)->fetchColumn();
            if(isset($_SESSION['customer']['cid'])) {
                $cid = $_SESSION['customer']['cid'];
                $cname = $_SESSION['customer']['cname'];
            }
            else {
                $cid = NULL;
                $cname = NULL;
            }
                
            return $this->c->view->render($response ,'Webpage/productdetails.twig', [
                'cart' => $cart,
                'cid' => $cid,
                'cname' => $cname,
                'product' => $product,
                'releatedProducts' => $releatedProducts,
                'reviews' => $reviews,
                'ordered' => $ordered
            ]);
        }

        public function autocomplete($request, $response, $args) {
            $params = $request->getParams();
            $searchtext = $params["term"];
            $sql = "select productName from product where productName like '%$searchtext%' LIMIT 7";
            $row = $this->c->db->query($sql)->fetchAll(PDO::FETCH_COLUMN);
            $result = json_encode($row);
            return $result;
        }

        public function productsearchurlstring($request, $response, $args) {
            $params = $request->getParams();
            $searchText = $params["searchText"];
            $productType = $params["productType"];
            $offset = 0;
            $url = $this->c->router->pathFor('product.productsearch')."?productcategory=".$productType."&searchfield=".$searchText;

            return  $url;
        }
        public function productsearch($request, $response, $args) {
            $params = $request->getParams();
            $searchText = $params["searchfield"];
            $productType = $params["productcategory"] == 'ALL CATEGORIES' ? '' : $params["productcategory"];
            $offset = 0;
            $sql = "select count(product.productId) from product where product.productName like 
                    '%$searchText%' or productType = '%$productType%' and productActiveStatus = '1' order by 
                    createdAt DESC ";
            $count = $this->c->db->query($sql)->fetchColumn();
            if($searchText != "" & $productType != '') {
                $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                    review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                    where product.productName like '%$searchText%' and productType like '%$productType%' and 
                    productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
            }
            elseif($searchText != "" & $productType == '') {
                $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                    review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                    where product.productName like '%$searchText%' and 
                    productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
            }
            elseif ($searchText == "" & $productType != '') {
                $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                    review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                    where productType like '%$productType%' and 
                    productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";

            }
            else {
                $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                    review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                    where product.productName like '%$searchText%' or productType like '%$productType%' and 
                    productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
            }
            $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            // var_dump($products);
            // die();
            
            if(isset($_SESSION['customer']['cid'])) {
                $cid = $_SESSION['customer']['cid'];
                $cname = $_SESSION['customer']['cname'];
            }
            else {
                $cid = NULL;
                $cname = NULL;
            }
            
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            $productType = $productType == '' ? 'ALL CATEGORIES' : $productType;

            //print_r(json_encode($np));
            //die();
            // return $response->withRedirect($this->c->router->pathFor('order.productsearch', [
            //     'cname' => $cname,
            //     'cid' => $cid,
            //     'cart' => $cart,
            //     'searchText' => $searchText,
            //     'productType' => $productType,
            //     'count' => $count,
            //     'products' => $products
            // ]));
            return $this->c->view->render($response,'Webpage/productsearch.twig',[
                'cname' => $cname,
                'cid' => $cid,
                'cart' => $cart,
                'searchText' => $searchText,
                'productType' => $productType,
                'count' => $count,
                'products' => $products
            ]);
        }

        public function productsearchbyspecifics($request, $response, $args) {
            if(isset($_SESSION["cart"]))
                $cart = count($_SESSION["cart"]);
            else
                $cart = 0;

            if(isset($_SESSION['customer']['cid'])) {
                $cid = $_SESSION['customer']['cid'];
                $cname = $_SESSION['customer']['cname'];
            }
            else {
                $cid = NULL;
                $cname = NULL;
            }

            $params = $request->getParams();
            
            $carmake = isset($params['carmake']) ? $params["carmake"] : '' ;
            $carmodel = isset($params['carmodel']) ? $params["carmodel"] : '' ;
            $producttype = isset($params['producttype']) ? $params["producttype"] : '' ;

            if($producttype != '')
                $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                        (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                        review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                        where product.productType like '$producttype' or product.productType like '%UNIVERSAL%' and 
                        productActiveStatus = '1' ";
            else 
                $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                        (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                        review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                        where product.productCarMake like '%$carmake%' and product.productCarModel like '%$carmodel%' or
                        product.productType like '%UNIVERSAL%' and productActiveStatus = '1' ";
            

            $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $response->withRedirect($this->c->router->pathFor('Webpage/productsearch.twig', [
                'cname' => $cname,
                'cid' => $cid,
                'cart' => $cart,
                'carmake' => $carmake,
                'carmodel' => $carmodel,
                'products' => $products
            ]));

            // return $this->c->view->render($response,'Webpage/productsearch.twig',[
                
            // ]);
        }

        public function productsearchddf($request, $response, $args) {
            $params = $request->getParams();
            $start = isset($params['start']) ? $params['start'] : 0;
            $length = isset($params['length']) ? $params['start'] : 10;
            
            $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                    review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                    where product.productName like '%%' and productActiveStatus = '1' order by createdAt,
                     RAND()  DESC";

            $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            

            $json_data = array(
                "draw" => $_GET['draw'],
                "recordsTotal" => 10,
                "recordsFiltered" => 10,
                "data" => $products,
                "deferLoading" => 10,
            );
            echo json_encode($json_data);
        }
        public function psearch($request, $response, $args) {
            $params = $request->getParams();
            $inital = isset($args['initial']) ? $args['initial'] : 0;
            if($inital == 0 ) {
                $searchText = isset($args['searchtext']) ? $args['searchtext'] != 'none' ? $args['searchtext'] : '' : '';
                $productType = isset($args['producttype']) ? $args['producttype'] != 'none' ? $args['producttype']: '' : '';
                $offset = isset($args['offset']) ? $args['offset'] : 0;
                if($searchText != "" & $productType != '') {
                    $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                        (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                        review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                        where product.productName like '%$searchText%' and productType like '%$productType%' and 
                        productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
                }
                elseif($searchText != "" & $productType == '') {
                    $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                        (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                        review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                        where product.productName like '%$searchText%' and 
                        productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
                }
                elseif ($searchText == "" & $productType != '') {
                    $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                        (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                        review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                        where productType like '%$productType%' and 
                        productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
    
                }
                else {
                    $sql = "select product.productId, productName, productQuantity, productMRP, productSP, productImage1, 
                    (select AVG(review.reviewRating) from review where review.productId = product.productId and 
                    review.approvalStatus = '1') as avg, REPLACE(productName, ' ', '-') as productURL from product 
                    where product.productName like '%$searchText%' or productType like '%$productType%' and 
                    productActiveStatus = '1' order by createdAt DESC limit 9 OFFSET $offset";
                }
                
                $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
                
                echo json_encode($products);
            }
            else {
                $data = [];
                echo json_encode($data);
            }
        }

    }