<?php

    namespace App\Controllers;

    use PDO;

    class ReviewController extends Controller{

        public function addreview($request, $response) {
            $params = $request->getParams();
            $cid = $params["cid"];
            $pid = $params["pid"];
            $cname = $params["cname"];
            $reviewText = $params["reviewText"];
            $rating = $params["rating"];

            $sql = "insert into review (reviewCustomerName, reviewRating, reviewMessage, approvalStatus, customerId, productId) 
                    values ('$cname','$rating','$reviewText', '0','$cid','$pid')";
            $this->c->db->exec($sql);
        }

        public function managereviews($request, $response, $args) {
            $sql = "select productId, productName, productSP, productImage1, (select AVG(review.reviewRating) from review where review.productId = product.productId) as avg, (select COUNT(review.reviewRating) from review where review.productId = product.productId) as totalreviews from product";
            $products = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response ,"/AdminPanel/managereviews.twig", [
                'products' => $products
            ]);
        }

        public function adminviewreviews($request, $response, $args) {
            $productId = $args['id'];
            $sql = "select * from review where productId = '$productId'";
            $reviews = $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ);
            return $this->c->view->render($response, "/AdminPanel/viewreview.twig", [
                'productId' => $productId,
                'reviews' => $reviews
            ]);
        }

        public function adminaddreview($request, $response, $args) {
            $productId = $args["id"];
            return $this->c->view->render($response, "/AdminPanel/addreviewview.twig", [
                'productId' => $productId
            ]);
        }

        public function adminpostreview($request, $response, $args) {
            $params = $request->getParams();
            $reviewCustomerName = $params['cname'];
            $reviewMessage = $params['message'];
            $reviewRating = $params['rating'];
            $customerId = uniqid('A_G-', true);
            $productId = $params['pid'];
            $sql = "insert into review (reviewCustomerName, reviewRating, reviewMessage, approvalStatus, customerId, productId) values 
                    ('$reviewCustomerName', '$reviewRating', '$reviewMessage', '1', '$customerId', '$productId')";
            $this->c->db->exec($sql);
            return $response->withRedirect($this->c->router->pathFor('review.adminviewreviews', [ 'id' => $productId ]));
        }

        public function deletereview($request, $response, $args) {
            $params = $request->getParams();
            $productId = $params["productId"];
            $customerId = $params["customerId"];
            $sql = "delete from review where productId = '$productId' and customerId = '$customerId'";
            //die();
            $this->c->db->exec($sql);
        }

        public function toggleapprovereview($request, $response, $args) {
            $params = $request->getParams();
            $customerId = $params["customerid"];
            $productId = $params["productid"];
            $approvalStatus = $params["approvalstatus"];
            $approvalStatus == 0 ? $approvalStatus = 1 : $approvalStatus = 0;
            $sql = "update review set approvalStatus = '$approvalStatus' where customerId = '$customerId' and productId = '$productId'";
            $this->c->db->exec($sql);
            $sql = "select approvalStatus from review where customerId = '$customerId' and productId = '$productId'";
            $reviewApprovalStatus = $this->c->db->query($sql)->fetchColumn();
            return json_encode($reviewApprovalStatus);
        }
    }