<?php

namespace App\Middleware;

use Firebase\JWT\JWT;
use \Tuupola\Base62;
use \Interop\Container\ContainerInterface as ContainerInterface;


class IfNewAuthenticatedMiddleware {

    private $c;
    private $key = "23607369AE334DFC7F244A172885249A";  // C@rm3t!c$-K3Y

    public function __construct(ContainerInterface $c) {
        //  var_dump($c);
        //  die();
        $this->c = $c;
    }

    public function __invoke($request, $response, $next)
    {
        $response = $next($request, $response);

        $now = new \DateTime();
        $future = new \DateTime("now +7 days");
        //$jti = Base62::encode(random_bytes(16));

        $payload = [
            "cid" => $_SESSION['customer']['cid'],
            "cname" => $_SESSION['customer']['cname'],
            "iat" => time(),
            "nbf" => time()
        ];
        $token = JWT::encode($payload, $this->key, "HS256");
        $startDate = time();
        $expDate = date('Y-m-d H:i:s', strtotime('+30 days', $startDate));
        
        setcookie('AccessToken', $token, time() + (86400 * 30), "/");
        var_dump($token);
        $response = $response->withRedirect($this->c->router->pathFor('customer.home'));
        //header('Location: ' . $_SERVER['HTTP_REFERER']);

        return $response;
    }
    
}