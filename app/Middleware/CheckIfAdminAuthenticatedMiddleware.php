<?php

namespace App\Middleware;

use Firebase\JWT\JWT;
use \Tuupola\Base62;
use \Interop\Container\ContainerInterface as ContainerInterface;


class CheckIfAdminAuthenticatedMiddleware {

    private $c;
    private $key = "BD7B083D76A5A0DE28AD64843370DD56"; // C@rm3t!c$-@dm!n-K3Y
    public function __construct(ContainerInterface $c) {
        $this->c = $c;
    }

    public function __invoke($request, $response, $next)
    {
        if (isset($_COOKIE["AdminAccessToken"])) {
            $token = $_COOKIE["AdminAccessToken"];
            $decoded = JWT::decode($token, $this->key, array('HS256'));
            $decoded_array = (array) $decoded;
            $aid = $decoded_array['aid'];
            //die();
            $_SESSION['admin']['aid'] = $aid;
            $sql = "select count(*) from admin where adminId = '$aid'";
            $row = $this->c->db->query($sql)->fetchColumn();
            if($row != 1) {
                setcookie('AdminAccessToken', '' , time()-60*60*24*365);
                $response = $response->withRedirect($this->c->router->pathFor('adminauth.login'));
            }
            $request->withAttribute('Adminid', $_SESSION['admin']['aid']);
        }
        else {
            $response = $response->withRedirect($this->c->router->pathFor('adminauth.login'));
        }
        return $next($request, $response);
    }
}