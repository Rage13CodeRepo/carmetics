<?php

namespace App\Middleware;

use Firebase\JWT\JWT;
use \Tuupola\Base62;
use \Interop\Container\ContainerInterface as ContainerInterface;


class IfAdminAuthenticatedMiddleware {

    private $c;
    private $key = "BD7B083D76A5A0DE28AD64843370DD56"; // C@rm3t!c$-@dm!n-K3Y

    public function __construct(ContainerInterface $c) {
        //  var_dump($c);
        //  die();
        $this->c = $c;
    }

    public function __invoke($request, $response, $next)
    {
        $response = $next($request, $response);

        $now = new \DateTime();
        $future = new \DateTime("now +7 days");
        //$jti = Base62::encode(random_bytes(16));

        if($_SESSION['admin']['aid'] == NULL) {
            $response = $response->withRedirect($this->c->router->pathFor('adminauth.login'));
        }
        else {
            $payload = [
                "aid" => $_SESSION['admin']['aid'],
                "aname" => $_SESSION['admin']['aname'],
                "iat" => time(),
                "nbf" => time()
            ];
            $token = JWT::encode($payload, $this->key, "HS256");
            $startDate = time();
            
            $expDate = date('Y-m-d H:i:s', strtotime('+30 days', $startDate));
            
            setcookie('AdminAccessToken', $token, time() + (86400 * 30), "/");
            //var_dump($token);
            $response = $response->withRedirect($this->c->router->pathFor('admin.index'));
        }

        
        //header('Location: ' . $_SERVER['HTTP_REFERER']);

        return $response;
    }
    
}