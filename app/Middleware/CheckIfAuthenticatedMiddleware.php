<?php

namespace App\Middleware;

use Firebase\JWT\JWT;
use \Tuupola\Base62;
use \Interop\Container\ContainerInterface as ContainerInterface;


class CheckIfAuthenticatedMiddleware {

    private $c;
    private $key = "23607369AE334DFC7F244A172885249A"; // C@rm3t!c$-K3Y
    public function __construct(ContainerInterface $c) {
        $this->c = $c;
    }

    public function __invoke($request, $response, $next)
    {
        if (isset($_COOKIE["AccessToken"])) {
            $token = $_COOKIE["AccessToken"];
            $decoded = JWT::decode($token, $this->key, array('HS256'));
            $decoded_array = (array) $decoded;
            $cid = $decoded_array['cid'];
            $cname = $decoded_array['cname'];
            $_SESSION['customer']['cid'] = $cid;
            $_SESSION['customer']['cname'] = $cname;
            $sql = "select count(*) from customer where customerId = '$cid'";
            $row = $this->c->db->query($sql)->fetchColumn();
            if($row != 1) {
                setcookie('AccessToken', '' , time()-60*60*24*365);
                $response = $response->withRedirect($this->c->router->pathFor('customer.login'));
            }
            $request->withAttribute('Userid', $_SESSION['customer']['cid']);
        }
        else {
            $response = $response->withRedirect($this->c->router->pathFor('customer.login'));
        }
        return $next($request, $response);
    }
}