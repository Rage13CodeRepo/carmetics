<?php 

    use Slim\Http\UploadedFile;
    use Interop\Container\ContainerInterface as ContainerInterface;

    $app = new \Slim\App([
        'settings' => [
            'displayErrorDetails' => true // set this to true in development env to show full and detailed error reports
        ]
    ]);

    $c = $app->getContainer();

    $container = $app->getContainer();

    $container['db'] = function() {
        return new PDO('mysql:host=localhost;dbname=carmetics','root','rage13');
    };

    $container['config'] = function() {
        return $config = (object) include("config.php");
    };

    $container['view'] = function ($container) {
        $view = new \Slim\Views\Twig(__DIR__.'/../views', [
            'debug' => true,
            'cache' => false
        ]);
        
        // Instantiate and add Slim specific extension

        $router = $container->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
        
        $view->getEnvironment()->addGlobal('base_url', $uri);

        // if(isset($_SESSION['customer'])) {
        //     $customer = array(
        //         'id' => $_SESSION['customer']['cid'],
        //         'name' => $_SESSION['customer']['cname'],
        //     );
        // }
        // else {
        //     $customer = array(
        //         'id' => '',
        //         'name' => '',
        //     );
        // }

        // $cart = array(
        //     'cart' => count($_SESSION["cart"])
        // );


        //$view->getEnvironment()->addGlobal('customer', $customer);
        // $view->getEnvironment()->addGlobal('customer', $cart);
        
        return $view;
    };
    
    //require __DIR__ .'/../app/Controllers/Middleware/IfAuthenticatedMiddleware.php';
    require __DIR__ .'/../routes/web.php';
     
