$(function() {
    "use strict";

        var tbl = $('#productsList').DataTable({"ordering": false, "bLengthChange": false, "bInfo": false});

        $('#search').keyup(function () {
            console.log($(this).val())
            tbl.search($(this).val()).draw();
        });

        $(".confirm-btn-alert").click(function () {

            swal({title: "Are you sure?", text: "Once deleted, you will not be able to recover this Product", icon: "warning", buttons: true, dangerMode: true}).then((willDelete) => {
                if (willDelete) {
                    swal("The product has been deleted!", {icon: "success"});
                } else {
                    swal("The product was not deleted");
                }
            });

        });
        

});