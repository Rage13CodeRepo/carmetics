// ({
//     baseUrl: '',
  
//     out: '../public/combine',
    
//     name: 'build_js',
//     wrap: true,

    
//     paths: {
//         jquery: 'public/adminpanel/js/jquery.min',
//         popper: 'public/adminpanel/js/popper.min',
//         simplebar: 'public/adminpanel/plugins/simplebar/js/simplebar',
//         sidebarmenu: 'public/adminpanel/js/sidebar-menu',
//         appscript: 'public/adminpanel/js/app-script',
//         chart: 'public/adminpanel/plugins/Chart.js/Chart.min',
//         jqueryjvectormap: 'public/adminpanel/plugins/vectormap/jquery-jvectormap-2.0.2.min',
//         jqueryjvectormapworld: 'public/adminpanel/plugins/vectormap/jquery-jvectormap-world-mill-en',
//         easypiechart: 'public/adminpanel/plugins/jquery.easy-pie-chart/jquery.easypiechart.min',
//         sparkline: 'public/adminpanel/plugins/sparkline-charts/jquery.sparkline.min',
//         excanvas: 'public/adminpanel/plugins/jquery-knob/excanvas',
//         dropzone: 'public/adminpanel/plugins/dropzone/js/dropzone',
//         jqueryknob: 'public/adminpanel/plugins/jquery-knob/jquery.knob',
//         jquerydataTables: 'public/adminpanel/plugins/bootstrap-datatable/js/jquery.dataTables.min',
//         dataTablesbootstrap4: 'public/adminpanel/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min',
//         dataTablesbuttons: 'public/adminpanel/plugins/bootstrap-datatable/js/dataTables.buttons.min',
//         buttonsbootstrap4: 'public/adminpanel/plugins/bootstrap-datatable/js/buttons.bootstrap4.min',
//         jszip: 'public/adminpanel/plugins/bootstrap-datatable/js/jszip.min',
//         pdfmake: 'public/adminpanel/plugins/bootstrap-datatable/js/pdfmake.min',
//         vfsfonts: 'public/adminpanel/plugins/bootstrap-datatable/js/vfs_fonts',
//         buttonshtml5: 'public/adminpanel/plugins/bootstrap-datatable/js/buttons.html5.min',
//         buttonsprint: 'public/adminpanel/plugins/bootstrap-datatable/js/buttons.print.min',
//         colVis: 'public/adminpanel/plugins/bootstrap-datatable/js/buttons.colVis.min',
//         sweetalert: 'public/adminpanel/plugins/alerts-boxes/js/sweetalert.min',
//         sweetalertscript: 'public/adminpanel/plugins/alerts-boxes/js/sweet-alert-script',
//         fancybox: 'public/adminpanel/plugins/fancybox/js/jquery.fancybox.min',
//         jspdf: 'public/adminpanel/js/jspdf.min',
//         html2canvas: 'public/adminpanel/js/html2canvas',
//         bootstrap: 'public/adminpanel/js/bootstrap.min',
//         index: 'public/adminpanel/js/index',
//         admin: 'public/adminpanel/js/admin',
//     },

//     //optimize: 'uglify',
//     removeCombined: false,
//     preserveLicenseComments: true,
// })


({
    appDir: "../public/adminpanel",
    baseUrl: ".",
    dir: "../public/tools",
    mainConfigFile: '../public/adminpanel/js/config_require.js',
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: '../../tools/bundle',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: [
                'jquery',
                'popper',
                // 'bootstrap',
                // 'simplebar',
                // 'sidebarmenu',
                // 'appscript',
                // 'chart',
                // 'jqueryjvectormap',
                // 'jqueryjvectormapworld',
                // 'easypiechart',
                // 'sparkline',
                // 'excanvas',
                // 'dropzone',
                // 'buttonsprint',
                // 'colVis',
                // 'jqueryknob',
                // 'jquerydataTables',
                // 'dataTablesbootstrap4',
                // 'dataTablesbuttons',
                // 'buttonsbootstrap4',
                // 'jszip',
                // 'pdfmake',
                // 'vfsfonts',
                // 'buttonshtml5',
                // 'sweetalert',
                // 'sweetalertscript',
                // 'fancybox',
                // 'jspdf',
                // 'html2canvas',
                // 'index',
                // 'admin'
            ]
    }],

    removeCombined: false,
    wrap: true,
    optimize: 'uglify2',

})