-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 21, 2019 at 05:44 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carmetics`
--

use carmetics;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `getCustomerById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCustomerById` (IN `cid` VARCHAR(255))  select * from customer where customerId = cid$$

DROP PROCEDURE IF EXISTS `getProductById`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getProductById` (IN `pid` VARCHAR(255))  select * from product where productId = pid$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `adminId` varchar(200) NOT NULL,
  `adminName` varchar(50) NOT NULL,
  `adminEmail` varchar(100) NOT NULL,
  `adminContactNumber` varchar(100) NOT NULL,
  `adminPassword` varchar(50) NOT NULL,
  PRIMARY KEY (`adminId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminId`, `adminName`, `adminEmail`, `adminContactNumber`, `adminPassword`) VALUES
('ADMIN-5ce50bce05e231.92432034', 'Admin', 'admin@carmetics.in', '9850670654', '0e7517141fb53f21ee439b355b5a1d0a');

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
CREATE TABLE IF NOT EXISTS `bill` (
  `invoiceNo` varchar(255) NOT NULL,
  `paymentId` varchar(255) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `paymentGatewayOrderId` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoiceNo`),
  KEY `orderId` (`orderId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courierdetails`
--

DROP TABLE IF EXISTS `courierdetails`;
CREATE TABLE IF NOT EXISTS `courierdetails` (
  `cdId` int(15) NOT NULL AUTO_INCREMENT,
  `carrier` varchar(255) NOT NULL,
  `shippingService` varchar(255) NOT NULL,
  `trackingId` varchar(255) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cdId`),
  KEY `orderId` (`orderId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customerId` varchar(255) NOT NULL,
  `customerName` varchar(100) NOT NULL,
  `customerUsername` varchar(100) NOT NULL,
  `customerEmail` varchar(100) NOT NULL,
  `customerContactNumber` varchar(12) DEFAULT NULL,
  `customerAddress` text,
  `customerPassword` varchar(255) DEFAULT NULL,
  `authProvider` varchar(20) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customerId`,`customerEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerId`, `customerName`, `customerUsername`, `customerEmail`, `customerContactNumber`, `customerAddress`, `customerPassword`, `authProvider`, `createdAt`, `updatedAt`) VALUES
('132655101211942', 'Saish Ssk', 'Saish Ssk', 'srage13@gmail.com', '9850670654', 'Shelar Building, Mohannagar', NULL, 'Facebook', '2019-04-22 15:37:19', '2019-04-22 15:37:19'),
('110279648090273083153', 'Saish S S Karapurkar', 'Saish SSK', 'ssksaish12@gmail.com', '9850670654', 'Shelar Building Flat 4, 2nd Floor, Mohannagar, Chinchwad, Pune, Maharashtra 411019', NULL, 'Google', '2019-04-25 17:55:18', '2019-04-25 17:55:18'),
('5cdc09820f3fc8.35200050', 'Test User', 'Test User', 'saish.sk@outlook.com', '8408087900', 'Some Big address', '2138cb5b0302e84382dd9b3677576b24', 'carmetics', '2019-05-15 18:13:46', '2019-05-15 18:13:46');

-- --------------------------------------------------------

--
-- Table structure for table `customorder`
--

DROP TABLE IF EXISTS `customorder`;
CREATE TABLE IF NOT EXISTS `customorder` (
  `customOrderId` varchar(255) NOT NULL,
  `customText` varchar(100) NOT NULL,
  `customOrderImage1` varchar(100) DEFAULT NULL,
  `customOrderImage2` varchar(100) DEFAULT NULL,
  `customOrderImage3` varchar(100) DEFAULT NULL,
  `customOrderImage4` varchar(100) DEFAULT NULL,
  `customerId` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customOrderId`),
  KEY `customerId` (`customerId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `msgId` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `customerId` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`msgId`),
  KEY `customerId` (`customerId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `orderId` varchar(255) NOT NULL,
  `orderStatus` varchar(50) NOT NULL,
  `orderShippingAddress` text,
  `orderAmount` int(6) NOT NULL,
  `customerId` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`orderId`),
  KEY `customerId` (`customerId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderscomprises`
--

DROP TABLE IF EXISTS `orderscomprises`;
CREATE TABLE IF NOT EXISTS `orderscomprises` (
  `ocId` varchar(255) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `productId` varchar(100) NOT NULL,
  `productSP` varchar(10) NOT NULL,
  `productQuantity` int(3) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ocId`,`orderId`,`productId`),
  KEY `productId` (`productId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `productId` varchar(100) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `productType` varchar(255) NOT NULL,
  `productQuantity` varchar(255) NOT NULL,
  `productMRP` varchar(255) NOT NULL,
  `productSP` varchar(10) NOT NULL,
  `productColor` varchar(100) NOT NULL,
  `productMaterialType` varchar(100) NOT NULL,
  `productHSNCode` varchar(100) NOT NULL,
  `productActiveStatus` varchar(100) NOT NULL,
  `productGSTTaxRate` varchar(100) NOT NULL,
  `productCarMake` varchar(100) NOT NULL,
  `productCarModel` varchar(100) NOT NULL,
  `productDescription` text NOT NULL,
  `universal` tinytext NOT NULL,
  `3DLetter` tinytext NOT NULL,
  `3DEmblem` tinytext NOT NULL,
  `sportsTeamStciker` tinytext NOT NULL,
  `doorSticker` tinytext NOT NULL,
  `windshieldSticker` tinytext NOT NULL,
  `windowsSticker` tinytext NOT NULL,
  `spiritualSticker` tinytext NOT NULL,
  `bikeSticker` tinytext NOT NULL,
  `productBulletPoint1` text NOT NULL,
  `productBulletPoint2` text NOT NULL,
  `productBulletPoint3` text NOT NULL,
  `productBulletPoint4` text NOT NULL,
  `productBulletPoint5` text,
  `productImage1` varchar(255) NOT NULL,
  `productImage2` varchar(255) NOT NULL,
  `productImage3` varchar(255) NOT NULL,
  `productImage4` varchar(255) NOT NULL,
  `productImage5` varchar(255) DEFAULT NULL,
  `productImage6` varchar(255) DEFAULT NULL,
  `productImage7` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`productId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promotionaloffermessage`
--

DROP TABLE IF EXISTS `promotionaloffermessage`;
CREATE TABLE IF NOT EXISTS `promotionaloffermessage` (
  `pomId` int(5) NOT NULL AUTO_INCREMENT,
  `pomMessage` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pomId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE IF NOT EXISTS `review` (
  `reviewCustomerName` varchar(100) DEFAULT NULL,
  `reviewRating` float NOT NULL,
  `reviewMessage` varchar(255) NOT NULL,
  `approvalStatus` int(2) NOT NULL,
  `customerId` varchar(255) NOT NULL,
  `productId` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customerId`,`productId`),
  KEY `productId` (`productId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE IF NOT EXISTS `wishlist` (
  `customerId` varchar(255) NOT NULL,
  `productId` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customerId`,`productId`),
  KEY `productId` (`productId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
